<?php

namespace App\Repository;

use App\Entity\TodoItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TodoItem>
 *
 * @method Todos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todos[]    findAll()
 * @method Todos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodoItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TodoItem::class);
    }
    public function completedTodos(int $page): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM todos t
            WHERE t.completed = true
            ORDER BY t.id DESC
            LIMIT :page
            ';

            $resultSet = $conn->executeQuery($sql, ['page' => $page]);

        return $resultSet->fetchAllAssociative();
    }
    public function notCompletedTodos($page): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM todos t
            WHERE t.completed = false
            ORDER BY t.id DESC
            ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }
}
