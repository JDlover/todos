<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @extends ServiceEntityRepository<Page>
 *
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Page::class);
  }
  public function lastPage()
  {
    $qb = $this->createQueryBuilder('p')
      ->select('MAX(p.number)')
      ->getQuery()
      ->getSingleScalarResult();
    $query = $qb;

    return $query;
  }
  public function prevPage(int $number)
  {
    $qb = $this->createQueryBuilder('p')
      ->where('p.number = :number-1')
      ->setParameter('number', $number);

    $query = $qb->getQuery();

    $array = $query->getOneOrNullResult();
    return $array;
  }
  public function nextPage(int $number)
  {
    $qb = $this->createQueryBuilder('p')
      ->where('p.number = :number+1')
      ->setParameter('number', $number);

    $query = $qb->getQuery();

    $array = $query->getOneOrNullResult();
    return $array;
  }
  public function pagesAfter(int $number)
  {
    $qb = $this->createQueryBuilder('p')
      ->where('p.number > :number')
      ->setParameter('number', $number);

    $query = $qb->getQuery();

    $array = $query->getResult();
    dump($array);
    return $array;
  }
}
